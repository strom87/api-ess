package middlewares

import (
	"bitbucket.org/strom87/api-ess/config"
	"bitbucket.org/strom87/api-ess/core/authentication"
	"bitbucket.org/strom87/api-ess/database"
	"bitbucket.org/strom87/api-ess/models"
	"encoding/json"
	"github.com/gorilla/context"
	"log"
	"net/http"
)

type Auth struct {
	Rsa          *config.Rsa
	userProvider *database.UserProvider
}

func NewAuth(u *database.UserProvider, r *config.Rsa) *Auth {
	return &Auth{r, u}
}

func (a Auth) ServeHTTP(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	valid, id, err := authentication.ValidateToken(r, a.userProvider, a.Rsa)
	if err != nil {
		log.Println(err.Error())
		a.notAuthenticatedMessage(w)
		return
	} else if !valid {
		a.notAuthenticatedMessage(w)
	}

	context.Set(r, "id", id)

	next(w, r)
}

func (a Auth) notAuthenticatedMessage(w http.ResponseWriter) {
	w.WriteHeader(http.StatusUnauthorized)
	json.NewEncoder(w).Encode(models.ResponseModel{Message: "Not authenticated"})
}
