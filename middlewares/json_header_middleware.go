package middlewares

import (
	"net/http"
)

type JsonHeader struct{}

func NewJsonHeader() *JsonHeader {
	return &JsonHeader{}
}

func (j JsonHeader) ServeHTTP(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	next(w, r)
}
