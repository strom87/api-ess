package account

import (
	"bitbucket.org/strom87/api-ess/core/encryption"
	"bitbucket.org/strom87/api-ess/core/helper"
	"bitbucket.org/strom87/api-ess/core/mail"
	"bitbucket.org/strom87/api-ess/database"
	"bitbucket.org/strom87/api-ess/database/entities"
	"bitbucket.org/strom87/api-ess/models"
	"github.com/strom87/validator-go"
	"gopkg.in/mgo.v2/bson"
	"log"
	"strings"
	"time"
)

func RegisterUser(model models.RegisterUserModel, userProvider *database.UserProvider, mailProvider *mail.MailProvider) models.ResponseModel {
	if isValid, messages, _ := validator.ValidateJson(&model); !isValid {
		return models.ResponseModel{Errors: messages}
	}

	exist, err := userProvider.EmailExists(model.Email)
	if err != nil {
		return errorResponse(err)
	}
	if exist {
		return models.ResponseModel{Message: "User already registered"}
	}

	password, salt := encryption.MakePassword(model.Password)

	user := newUserModel(model, password, salt)
	if err := userProvider.Insert(user); err != nil {
		return errorResponse(err)
	}

	inserterdUser, err := userProvider.FindByEmail(user.Email)
	if err != nil {
		return errorResponse(err)
	}

	token, _ := encryption.EncryptAes(string(inserterdUser.Id))

	mailProvider.Send("dev.ess.api@gmail.com", inserterdUser.Email, "Activation", "Token: "+token)

	return models.ResponseModel{Message: "User registered", Success: true}
}

func ActivateAccount(token string, userProvider *database.UserProvider) models.ResponseModel {
	decryptedId, err := encryption.DecryptAes(token)
	if err != nil {
		return models.ResponseModel{Message: "Invalid token"}
	}

	id := bson.ObjectId(decryptedId)

	exist, err := userProvider.IdExists(id)
	if err != nil {
		return errorResponse(err)
	}

	if exist {
		userProvider.ActivateAccount(id)
		return models.ResponseModel{Message: "Account activated", Success: true}
	}

	return models.ResponseModel{Message: "Invalid token"}
}

func ForgottenPassword(email string, userProvider *database.UserProvider, mailProvider *mail.MailProvider) models.ResponseModel {
	user, err := userProvider.FindByEmail(email)
	if err != nil {
		return errorResponse(err)
	}
	if user != nil {
		return models.ResponseModel{Message: "Invalid email"}
	}

	newPassword := helper.RandomString(10)
	password, salt := encryption.MakePassword(newPassword)

	user.Credential.Salt = salt
	user.Credential.Password = password

	if err := userProvider.Update(user); err != nil {
		return errorResponse(err)
	}

	mailProvider.Send("dev.ess.api@gmail.com", "daniel.strom.87@gmail.com", "Forgotten password", "New password: "+newPassword)

	return models.ResponseModel{Message: "New password sent", Success: true}
}

func newUserModel(model models.RegisterUserModel, password string, salt string) entities.UserEntity {
	return entities.UserEntity{
		FirstName:    model.FirstName,
		LastName:     model.LastName,
		Email:        strings.ToLower(model.Email),
		RegisterDate: time.Now(),
		Credential: entities.UserCredentialEntity{
			Salt:       salt,
			Password:   password,
			RehashDate: encryption.GetNewPasswordRehashDate(),
		},
	}
}

func errorResponse(err error) models.ResponseModel {
	log.Println(err.Error())
	return models.ResponseModel{Message: "Invalid input"}
}
