package encryption

import (
	"crypto/rand"
	"golang.org/x/crypto/bcrypt"
	"log"
	"strings"
	"time"
)

const (
	SaltLength  = 24
	EncryptCost = 12
	RehashDays  = 14
)

// Handles create a new hash/salt combo from a raw password as inputted by the user
func MakePassword(raw_password string) (string, string) {
	salt := generateSalt()
	salted_pass := combine(salt, raw_password)
	password := hashPassword(salted_pass)

	return password, salt
}

// Checks whether or not the correct password has been provided
func MatchPasswords(raw_password string, hashed_password string, salt string) bool {
	salted_guess := combine(salt, raw_password)

	return bcrypt.CompareHashAndPassword([]byte(hashed_password), []byte(salted_guess)) == nil
}

// Creates the date that the password should be rehased at
func GetNewPasswordRehashDate() time.Time {
	return time.Now().AddDate(0, 0, RehashDays)
}

// Checks if the date is passed and the password needs to be rehased
func PasswordRehashNeeded(rehash_date time.Time) bool {
	return time.Now().After(rehash_date)
}

// this handles taking a raw user password and making in into something safe for storing in our DB
func hashPassword(salted_password string) string {
	hashed_password, err := bcrypt.GenerateFromPassword([]byte(salted_password), EncryptCost)
	if err != nil {
		log.Fatal(err)
	}

	return string(hashed_password)
}

// Handles merging together the salt and the password
func combine(salt string, raw_password string) string {
	pieces := []string{salt, raw_password}
	salted_password := strings.Join(pieces, "")
	return salted_password
}

// Generates a random salt using DevNull
func generateSalt() string {
	salt := make([]byte, SaltLength)
	if _, err := rand.Read(salt); err != nil {
		log.Fatal(err)
	}

	return string(salt[:])
}
