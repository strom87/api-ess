package mail

import (
	"bitbucket.org/strom87/api-ess/config"
	"encoding/base64"
	"fmt"
	"log"
	ma "net/mail"
	"net/smtp"
	"strings"
)

type MailProvider struct {
	Port     string
	SmtpHost string
	User     string
	Password string
}

func NewMailProvider(s *config.Settings) *MailProvider {
	return &MailProvider{s.Mail.Port, s.Mail.SmtpHost, s.Mail.User, s.Mail.Password}
}

func (m MailProvider) Send(from, to, subject, body string) {
	f := ma.Address{"", from}
	t := ma.Address{"", to}

	auth := smtp.PlainAuth("", from, m.Password, m.SmtpHost)

	message := ""
	for k, v := range m.getHeaders(f, t, subject) {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}

	message += "\r\n" + base64.StdEncoding.EncodeToString([]byte(body))

	if err := smtp.SendMail(m.combindeSmtpHostAndPort(), auth, m.User, []string{to}, []byte(message)); err != nil {
		log.Println(err)
	}
}

func (m MailProvider) encodeRFC2047(String string) string {
	addr := ma.Address{String, ""}
	return strings.Trim(addr.String(), " <>")
}

func (m MailProvider) combindeSmtpHostAndPort() string {
	if m.Port == "" {
		return m.SmtpHost
	}

	return strings.Join([]string{m.SmtpHost, m.Port}, ":")
}

func (m MailProvider) getHeaders(from ma.Address, to ma.Address, subject string) map[string]string {
	header := map[string]string{}
	header["From"] = from.String()
	header["To"] = to.String()
	header["Subject"] = m.encodeRFC2047(subject)
	header["MIME-Version"] = "1.0"
	header["Content-Type"] = "text/plain; charset=\"utf-8\""
	header["Content-Transfer-Encoding"] = "base64"
	return header
}
