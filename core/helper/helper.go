package helper

import (
	"math/rand"
	"time"
)

var (
	runeString = []rune("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func RandomString(length int) string {
	str := make([]rune, length)
	for i := range str {
		str[i] = runeString[rand.Intn(len(runeString))]
	}
	return string(str)
}
