package authentication

import (
	"bitbucket.org/strom87/api-ess/config"
	"bitbucket.org/strom87/api-ess/core/encryption"
	"bitbucket.org/strom87/api-ess/core/helper"
	"bitbucket.org/strom87/api-ess/database"
	"bitbucket.org/strom87/api-ess/database/entities"
	"bitbucket.org/strom87/api-ess/models"
	crsa "crypto/rsa"
	"errors"
	jwt "github.com/dgrijalva/jwt-go"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

func Login(model models.LoginModel, userProvider *database.UserProvider) (bool, *models.ResponseModel) {
	user, err := userProvider.FindByEmail(model.Email)
	if err != nil {
		log.Println(err.Error())
		return false, &models.ResponseModel{Message: "Invalid email or password"}
	}
	if user == nil {
		return false, &models.ResponseModel{Message: "Invalid email or password"}
	}

	if !user.IsActivated {
		return false, &models.ResponseModel{Message: "Account not activated"}
	}

	if !encryption.MatchPasswords(model.Password, user.Credential.Password, user.Credential.Salt) {
		return false, &models.ResponseModel{Message: "Invalid username or password"}
	}

	if encryption.PasswordRehashNeeded(user.Credential.RehashDate) {
		if err := rehashPassword(model.Password, user, userProvider); err != nil {
			log.Println(err.Error())
		}
	}

	return true, nil
}

func TokenAndRefreshToken(email string, userProvider *database.UserProvider, rsa *config.Rsa) (*models.TokenModel, error) {
	user, err := userProvider.FindByEmail(email)
	if err != nil {
		return nil, err
	}

	token, tokenKey, err := Token(user, rsa)
	if err != nil {
		return nil, err
	}

	refreshToken, refreshTokenKey, err := RefreshToken(user, rsa)
	if err != nil {
		return nil, err
	}

	if err := setUserTokenKeys(entities.TokenKeyEntity{tokenKey, refreshTokenKey}, user, userProvider); err != nil {
		return nil, err
	}

	return &models.TokenModel{token, refreshToken}, nil
}

func Token(user *entities.UserEntity, rsa *config.Rsa) (string, string, error) {
	tokenKey := helper.RandomString(20)

	privateKey, err := ParsePrivateKey(rsa)
	if err != nil {
		return "", "", err
	}

	token, err := generateToken(user, privateKey, tokenKey)
	if err != nil {
		return "", "", err
	}

	return token, tokenKey, nil
}

func RefreshToken(user *entities.UserEntity, rsa *config.Rsa) (string, string, error) {
	tokenKey := helper.RandomString(20)

	privateKey, err := ParsePrivateKey(rsa)
	if err != nil {
		return "", "", err
	}

	token, err := generateRefreshToken(user, privateKey, tokenKey)
	if err != nil {
		return "", "", err
	}

	return token, tokenKey, nil
}

func ValidateToken(r *http.Request, userProvider *database.UserProvider, rsa *config.Rsa) (bool, string, error) {
	publicKey, err := ParsePublicKey(rsa)
	if err != nil {
		return false, "", err
	}

	token, err := jwt.ParseFromRequest(r, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, errors.New("Unexpected signing method: " + token.Header["alg"].(string))
		}
		if token.Claims["token_type"].(string) != "auth" {
			return nil, errors.New("Not a valid auth token")
		}

		expires, err := time.Parse(time.RFC3339, token.Claims["exp"].(string))
		if err != nil {
			return nil, err
		}
		if time.Now().After(expires) {
			return nil, errors.New("Token expired")
		}

		isValid, err := userProvider.ValidToken(token.Claims["id"].(string), token.Claims["key"].(string))
		if err != nil {
			return nil, errors.New("Not a valid token. " + err.Error())
		} else if !isValid {
			return nil, errors.New("Token key is not valid")
		}

		return publicKey, nil
	})

	return token.Valid, token.Claims["id"].(string), err
}

func ParsePrivateKey(rsa *config.Rsa) (*crsa.PrivateKey, error) {
	privateKeyBytes, err := ioutil.ReadFile(rsa.PrivateFilePath)
	if err != nil {
		return nil, err
	}

	privateKey, err := jwt.ParseRSAPrivateKeyFromPEM(privateKeyBytes)
	if err != nil {
		return nil, err
	}

	return privateKey, nil
}

func ParsePublicKey(rsa *config.Rsa) (*crsa.PublicKey, error) {
	publicKeyBytes, err := ioutil.ReadFile(rsa.PublicFilePath)
	if err != nil {
		return nil, err
	}

	publicKey, err := jwt.ParseRSAPublicKeyFromPEM(publicKeyBytes)
	if err != nil {
		return nil, err
	}

	return publicKey, nil
}

func generateToken(user *entities.UserEntity, privateKey *crsa.PrivateKey, key string) (string, error) {
	token := jwt.New(jwt.SigningMethodRS256)
	token.Claims["id"] = user.Id
	token.Claims["exp"] = time.Now().Add(time.Minute * 5)
	token.Claims["iat"] = time.Now()
	token.Claims["token_type"] = "auth"
	token.Claims["key"] = key

	tokenString, err := token.SignedString(privateKey)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func generateRefreshToken(user *entities.UserEntity, privateKey *crsa.PrivateKey, key string) (string, error) {
	token := jwt.New(jwt.SigningMethodRS256)
	token.Claims["id"] = user.Id
	token.Claims["exp"] = time.Now().Add(((time.Hour * 24) * 356) * 5)
	token.Claims["iat"] = time.Now()
	token.Claims["token_type"] = "refresh"
	token.Claims["key"] = key

	tokenString, err := token.SignedString(privateKey)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func rehashPassword(password string, user *entities.UserEntity, userProvider *database.UserProvider) error {
	pass, salt := encryption.MakePassword(password)
	user.Credential.Salt = salt
	user.Credential.Password = pass

	if err := userProvider.Update(user); err != nil {
		return err
	}

	return nil
}

func setUserTokenKeys(model entities.TokenKeyEntity, user *entities.UserEntity, userProvider *database.UserProvider) error {
	user.TokenKey.Key = model.Key
	user.TokenKey.RefreshKey = model.RefreshKey

	if err := userProvider.Update(user); err != nil {
		return err
	}

	return nil
}
