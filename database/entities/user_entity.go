package entities

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

type UserEntity struct {
	Id           bson.ObjectId        `json:"id" bson:"_id,omitempty"`
	FirstName    string               `json:"first_name" bson:"first_name"`
	LastName     string               `json:"last_name" bson:"last_name"`
	Email        string               `json:"email" bson:"email"`
	IsActivated  bool                 `json:"is_activated" bson:"is_activated"`
	RegisterDate time.Time            `json:"register_date" bson:"register_date"`
	Credential   UserCredentialEntity `json:"credential" bson:"credential"`
	TokenKey     TokenKeyEntity       `json:"token_key" bson:"token_key"`
}

func NewUser() *UserEntity {
	return &UserEntity{}
}
