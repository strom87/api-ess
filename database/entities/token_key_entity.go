package entities

type TokenKeyEntity struct {
	Key        string `json:"key" bson:"key"`
	RefreshKey string `json:"refresh_key" bson:"refresh_key"`
}
