package entities

import "time"

type UserCredentialEntity struct {
	Salt       string    `json:"salt" bson:"salt"`
	Password   string    `json:"password" bson:"password"`
	RehashDate time.Time `json:"rehash_date" bson:"rehash_date"`
}
