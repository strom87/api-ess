package database

import (
	"bitbucket.org/strom87/api-ess/config"
	"bitbucket.org/strom87/api-ess/database/entities"
	"gopkg.in/mgo.v2/bson"
	"strings"
)

type UserProvider struct {
	Db
}

func NewUserProvider(settings *config.Settings) *UserProvider {
	return &UserProvider{Db{settings}}
}

func (u UserProvider) Insert(user entities.UserEntity) error {
	db, session := u.Connect()
	defer u.Close(session)

	if err := db.C("users").Insert(&user); err != nil {
		return err
	}

	return nil
}

func (u UserProvider) Update(user *entities.UserEntity) error {
	db, session := u.Connect()
	defer u.Close(session)

	if err := db.C("users").Update(bson.M{"_id": user.Id}, user); err != nil {
		return err
	}

	return nil
}

func (u UserProvider) ActivateAccount(id bson.ObjectId) error {
	db, session := u.Connect()
	defer u.Close(session)

	if err := db.C("users").Update(bson.M{"_id": id}, bson.M{"$set": bson.M{"is_activated": true}}); err != nil {
		return err
	}

	return nil
}

func (u UserProvider) FindById(id bson.ObjectId) (*entities.UserEntity, error) {
	db, session := u.Connect()
	defer u.Close(session)

	result := &entities.UserEntity{}
	if err := db.C("users").Find(bson.M{"_id": id}).One(result); err != nil {
		return result, err
	}

	return result, nil
}

func (u UserProvider) FindByEmail(email string) (*entities.UserEntity, error) {
	db, session := u.Connect()
	defer u.Close(session)

	result := &entities.UserEntity{}

	if err := db.C("users").Find(bson.M{"email": email}).One(&result); err != nil {
		return result, err
	}

	return result, nil
}

func (u UserProvider) FindByName(name string) (*entities.UserEntity, error) {
	db, session := u.Connect()
	defer u.Close(session)

	result := &entities.UserEntity{}
	if err := db.C("users").Find(bson.M{"name": name}).One(&result); err != nil {
		return result, err
	}

	return result, nil
}

func (u UserProvider) IdExists(id bson.ObjectId) (bool, error) {
	db, session := u.Connect()
	defer u.Close(session)

	count, err := db.C("users").Find(bson.M{"_id": id}).Count()
	if err != nil {
		return false, err
	}

	return count != 0, nil
}

func (u UserProvider) EmailExists(email string) (bool, error) {
	db, session := u.Connect()
	defer u.Close(session)

	count, err := db.C("users").Find(bson.M{"email": strings.ToLower(email)}).Count()
	if err != nil {
		return false, err
	}

	return count != 0, nil
}

func (u UserProvider) ValidToken(id string, key string) (bool, error) {
	db, session := u.Connect()
	defer u.Close(session)

	query := bson.M{"_id": bson.ObjectIdHex(id), "token_key.key": key}
	count, err := db.C("users").Find(query).Count()
	if err != nil {
		return false, err
	}

	return count != 0, nil
}
