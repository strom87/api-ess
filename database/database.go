package database

import (
	"bitbucket.org/strom87/api-ess/config"
	"gopkg.in/mgo.v2"
)

type Db struct {
	settings *config.Settings
}

func (d Db) Connect() (*mgo.Database, *mgo.Session) {
	session, err := mgo.Dial(d.settings.Database.Connection)
	if err != nil {
		panic(err)
	}

	session.SetMode(mgo.Monotonic, true)
	return session.DB(d.settings.Database.Name), session
}

func (d Db) Close(session *mgo.Session) {
	session.Close()
}
