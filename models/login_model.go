package models

type LoginModel struct {
	Email    string `json:"email" validator:"email"`
	Password string `json:"password" validator:"min:6|max:50"`
}
