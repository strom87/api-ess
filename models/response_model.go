package models

type ResponseModel struct {
	Message string              `json:"message,omitempty"`
	Success bool                `json:"success"`
	Token   *TokenModel         `json:"tokens,omitempty"`
	Errors  map[string][]string `json:"errors,omitempty"`
}
