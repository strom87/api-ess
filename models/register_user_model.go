package models

type RegisterUserModel struct {
	FirstName    string `json:"first_name" validator:"required|alpha|max:30"`
	LastName     string `json:"last_name" validator:"required|alpha|max:30"`
	Email        string `json:"email" validator:"required|email"`
	Password     string `json:"password" validator:"min:6|max:50|equals:ConfPassword"`
	ConfPassword string `json:"conf_password"`
}
