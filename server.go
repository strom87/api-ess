package main

import (
	"bitbucket.org/strom87/api-ess/config"
	"bitbucket.org/strom87/api-ess/routes"
	"net/http"
)

var (
	settings *config.Settings
)

func init() {
	settings = config.NewSettings()
}

func main() {
	negroni := routes.InitRoutes(settings)
	http.ListenAndServe(":1337", negroni)
}
