package config

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
)

var (
	filePath = map[string]string{
		"dev":  "config/dev.json",
		"prod": "config/prod.json",
	}
)

type Settings struct {
	Mail     Mail     `json:"mail"`
	Database Database `json:"database"`
	Rsa      Rsa      `json:"rsa"`
}

type Database struct {
	Name       string `json:"name"`
	Connection string `json:"connection"`
}

type Mail struct {
	Port     string `json:"port"`
	SmtpHost string `json:"smtp_host"`
	User     string `json:"user"`
	Password string `json:"password"`
}

type Rsa struct {
	PublicFilePath  string `json:"public_file_path"`
	PrivateFilePath string `json:"private_file_path"`
}

func NewSettings() *Settings {
	env := os.Getenv("GOENV")
	if env == "" {
		env = "dev"
	}

	file, err := ioutil.ReadFile(filePath[env])
	if err != nil {
		log.Println(err)
	}

	settings := Settings{}
	err = json.Unmarshal(file, &settings)
	if err != nil {
		log.Println(err)
	}

	return &settings
}
