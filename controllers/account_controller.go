package controllers

import (
	"bitbucket.org/strom87/api-ess/core/account"
	"bitbucket.org/strom87/api-ess/core/mail"
	"bitbucket.org/strom87/api-ess/database"
	"bitbucket.org/strom87/api-ess/models"
	"encoding/json"
	"fmt"
	"net/http"
)

type AccountController struct {
	mailProvider *mail.MailProvider
	userProvider *database.UserProvider
}

func NewAccountController(u *database.UserProvider, m *mail.MailProvider) *AccountController {
	return &AccountController{m, u}
}

func (a AccountController) Register(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	model := models.RegisterUserModel{}
	if err := json.NewDecoder(r.Body).Decode(&model); err != nil {
		json.NewEncoder(w).Encode(&models.ResponseModel{Message: "Invalid input"})
		fmt.Println(err.Error())
		return
	}

	response := account.RegisterUser(model, a.userProvider, a.mailProvider)

	json.NewEncoder(w).Encode(&response)
}

func (a AccountController) ActivateAccount(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	model := models.ActivateAccountModel{}
	if err := json.NewDecoder(r.Body).Decode(&model); err != nil {
		json.NewEncoder(w).Encode(&models.ResponseModel{Message: "Invalid input"})
		fmt.Println(err.Error())
		return
	}

	response := account.ActivateAccount(model.Token, a.userProvider)

	json.NewEncoder(w).Encode(&response)
}

func (a AccountController) ForgottenPassword(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	model := models.EmailModel{}
	if err := json.NewDecoder(r.Body).Decode(&model); err != nil {
		json.NewEncoder(w).Encode(&models.ResponseModel{Message: "Invalid input"})
		fmt.Println(err.Error())
		return
	}

	response := account.ForgottenPassword(model.Email, a.userProvider, a.mailProvider)

	json.NewEncoder(w).Encode(&response)
}
