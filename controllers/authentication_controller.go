package controllers

import (
	"bitbucket.org/strom87/api-ess/config"
	"bitbucket.org/strom87/api-ess/core/authentication"
	"bitbucket.org/strom87/api-ess/database"
	"bitbucket.org/strom87/api-ess/models"
	"encoding/json"
	"fmt"
	"github.com/gorilla/context"
	"log"
	"net/http"
)

type AuthenticationController struct {
	Rsa          *config.Rsa
	userProvider *database.UserProvider
}

func NewAuthenticationController(u *database.UserProvider, s *config.Rsa) *AuthenticationController {
	return &AuthenticationController{s, u}
}

func (a AuthenticationController) Login(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	model := models.LoginModel{}
	if err := json.NewDecoder(r.Body).Decode(&model); err != nil {
		log.Println(err.Error())
		json.NewEncoder(w).Encode(&models.ResponseModel{Message: "Invalid input"})
		return
	}

	if isValid, response := authentication.Login(model, a.userProvider); !isValid {
		json.NewEncoder(w).Encode(response)
		return
	}

	token, err := authentication.TokenAndRefreshToken(model.Email, a.userProvider, a.Rsa)
	if err != nil {
		log.Println(err.Error())
		json.NewEncoder(w).Encode(&models.ResponseModel{Message: "Invalid input"})
		return
	}

	json.NewEncoder(w).Encode(models.ResponseModel{Success: true, Token: token})
}

func (a AuthenticationController) RefreshToken(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	// fmt.Fprintln(w, "Get a new token, when a valid refresh token is passed in")
}

func (a AuthenticationController) SignOut(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	fmt.Fprintln(w, "{\"test\": \""+context.Get(r, "id").(string)+"\"}")
}
