package routes

import (
	"bitbucket.org/strom87/api-ess/config"
	"bitbucket.org/strom87/api-ess/controllers"
	"bitbucket.org/strom87/api-ess/middlewares"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

func AuthenticationRoutes(router *mux.Router, rsa *config.Rsa, auth *middlewares.Auth) {
	ac := controllers.NewAuthenticationController(userProvider, rsa)
	router.Handle("/login", negroni.New(negroni.HandlerFunc(ac.Login)))
	router.Handle("/refresh-token", negroni.New(negroni.HandlerFunc(ac.RefreshToken)))
	router.Handle("/sign-out", negroni.New(auth, negroni.HandlerFunc(ac.SignOut)))
}
