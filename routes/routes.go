package routes

import (
	"bitbucket.org/strom87/api-ess/config"
	"bitbucket.org/strom87/api-ess/core/mail"
	"bitbucket.org/strom87/api-ess/database"
	"bitbucket.org/strom87/api-ess/middlewares"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

var (
	mailProvider   *mail.MailProvider
	userProvider   *database.UserProvider
	authMiddleware *middlewares.Auth
)

func initalize(settings *config.Settings) {
	mailProvider = mail.NewMailProvider(settings)
	userProvider = database.NewUserProvider(settings)
	authMiddleware = middlewares.NewAuth(userProvider, &settings.Rsa)
}

func InitRoutes(settings *config.Settings) *negroni.Negroni {
	initalize(settings)

	n := negroni.Classic()
	router := mux.NewRouter()

	n.Use(middlewares.NewJsonHeader())

	AccountRoutes(router)
	AuthenticationRoutes(router, &settings.Rsa, authMiddleware)

	n.UseHandler(router)
	return n
}
