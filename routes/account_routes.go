package routes

import (
	"bitbucket.org/strom87/api-ess/controllers"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

func AccountRoutes(router *mux.Router) {
	ac := controllers.NewAccountController(userProvider, mailProvider)
	router.Handle("/account/register", negroni.New(negroni.HandlerFunc(ac.Register)))
	router.Handle("/account/activate-account", negroni.New(negroni.HandlerFunc(ac.ActivateAccount)))
	router.Handle("/account/forgotten-password", negroni.New(negroni.HandlerFunc(ac.ForgottenPassword)))
}
